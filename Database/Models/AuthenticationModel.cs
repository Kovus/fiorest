﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class AuthenticationModel
    {
        public int AuthenticationModelId { get; set; }

        public bool AccountEnabled { get; set; }
        public string DisabledReason { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }

        public bool IsAdministrator { get; set; }

        public virtual List<PermissionAllowance> Allowances { get; set; } = new List<PermissionAllowance>();
        public virtual List<FailedLoginAttempt> FailedAttempts { get; set; } = new List<FailedLoginAttempt>();

        public Guid AuthorizationKey { get; set; }
        public DateTime AuthorizationExpiry { get; set; }
    }

    public class PermissionAllowance
    {
        [JsonIgnore]
        public int PermissionAllowanceId { get; set; }

        public string UserName { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }

    public class FailedLoginAttempt
    {
        [JsonIgnore]
        public int FailedLoginAttemptId { get; set; }

        [JsonIgnore]
        public int AuthenticationModelId { get; set; }

        public string Address { get; set; }

        public DateTime FailedAttemptDateTime { get; set; }

        [JsonIgnore]
        public virtual AuthenticationModel AuthenticationModel { get; set; }
    }
}

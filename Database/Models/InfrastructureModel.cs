﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class InfrastructureModel
    {
        [JsonIgnore]
        public int InfrastructureModelId { get; set; }

        public string PopulationId { get; set; }

        public virtual List<InfrastructureInfo> InfrastructureInfos { get; set; } = new List<InfrastructureInfo>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class InfrastructureInfo
    {
        [JsonIgnore]
        public int InfrastructureInfoId { get; set; }

        public string Type { get; set; }
        public string Ticker { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public int ActiveLevel { get; set; }
        public int CurrentLevel { get; set; }
        public double UpkeepStatus { get; set; }
        public double UpgradeStatus { get; set; }

        [JsonIgnore]
        public int InfrastructureModelId { get; set;}

        [JsonIgnore]
        public virtual InfrastructureModel InfrastructureModel { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class CXDataModel
    {
        [JsonIgnore]
        public int CXDataModelId { get; set; }

        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialId { get; set; }

        public string ExchangeName { get; set; }
        public string ExchangeCode { get; set; }

        public string Currency { get; set; }

        public double? Previous { get; set; }
        public double? Price { get; set; }
        public long? PriceTimeEpochMs { get; set; }

        public double? High { get; set; }
        public double? AllTimeHigh { get; set; }

        public double? Low { get; set; }
        public double? AllTimeLow { get; set; }

        public double? Ask { get; set; }
        public int? AskCount { get; set; }

        public double? Bid { get; set; }
        public int? BidCount { get; set; }

        public int? Supply { get; set; }
        public int? Demand { get; set; }
        
        public int? Traded { get; set; }

        public double? VolumeAmount { get; set; }

        public double? PriceAverage { get; set; }

        public double? NarrowPriceBandLow { get; set; }
        public double? NarrowPriceBandHigh { get; set; }

        public double? WidePriceBandLow { get; set; }
        public double? WidePriceBandHigh { get; set; }

        public virtual List<CXBuyOrder> BuyingOrders { get; set; } = new List<CXBuyOrder>();
        public virtual List<CXSellOrder> SellingOrders { get; set; } = new List<CXSellOrder>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class CXBuyOrder
    {
        [JsonIgnore]
        public int CXBuyOrderId { get; set; }

        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [JsonIgnore]
        public int CXDataModelId { get; set; }
        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }
    }

    public class CXSellOrder
    {
        [JsonIgnore]
        public int CXSellOrderId { get; set; }

        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }

        public int? ItemCount { get; set; }
        public double ItemCost { get; set; }

        [JsonIgnore]
        public int CXDataModelId { get; set; }
        [JsonIgnore]
        public virtual CXDataModel CXDataModel { get; set; }
    }
}

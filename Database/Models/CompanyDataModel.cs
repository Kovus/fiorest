﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class CompanyDataModel
    {
        [JsonIgnore]
        public int CompanyDataModelId { get; set; }

        public string UserName { get; set; }
        public string HighestTier { get; set; }
        public bool Pioneer { get; set; }
        public bool Team { get; set; }

        public long CreatedEpochMs { get; set; }

        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }

        public string CountryId { get; set; }

        public string CurrencyCode { get; set; }

        public string StartingProfile { get; set; }
        public string StartingLocation { get; set; }

        public virtual List<CompanyDataCurrencyBalance> Balances { get; set; } = new List<CompanyDataCurrencyBalance>();

        public string OverallRating { get; set; }
        public string ActivityRating { get; set; }
        public string ReliabilityRating { get; set; }
        public string StabilityRating { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class CompanyDataCurrencyBalance
    {
        [JsonIgnore]
        public int CompanyDataCurrencyBalanceId { get; set; }

        public string Currency;
        public double Balance;

        [JsonIgnore]
        public int CompanyDataModelId { get; set; }

        [JsonIgnore]
        public virtual CompanyDataModel CompanyDataModel { get; set; }
    }
}

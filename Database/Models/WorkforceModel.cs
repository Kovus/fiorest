﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class WorkforceModel
    {
        [JsonIgnore]
        public int WorkforceModelId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string SiteId { get; set; }

        public virtual List<WorkforceDescription> Workforces { get; set; } = new List<WorkforceDescription>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class WorkforceDescription
    {
        [JsonIgnore]
        public int WorkforceDescriptionId { get; set; }

        public string WorkforceTypeName { get; set; }
        public int Population { get; set; }
        public int Reserve { get; set; }
        public int Capacity { get; set; }
        public int Required { get; set; }

        public double Satisfaction { get; set; }

        public virtual List<WorkforceNeed> WorkforceNeeds { get; set; } = new List<WorkforceNeed>();

        [JsonIgnore]
        public int WorkforceModelId { get; set; }
        [JsonIgnore]
        public virtual WorkforceModel WorkforceModel { get; set; }
    }

    public class WorkforceNeed
    {
        [JsonIgnore]
        public int WorkforceNeedId { get; set; }

        public string Category { get; set; }
        public bool Essential { get; set; }
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }

        public double Satisfaction { get; set; }
        public double UnitsPerInterval { get; set; }
        public double UnitsPerOneHundred { get; set; }

        [JsonIgnore]
        public int WorkforceDescriptionId { get; set; }
        [JsonIgnore]
        public virtual WorkforceDescription WorkforceDescription { get; set; }
    }
}

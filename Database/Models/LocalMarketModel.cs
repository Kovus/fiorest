﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIORest.Database.Models
{
    public class LocalMarketModel
    {
        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        public string MarketId { get; set; }

        public virtual List<ShippingAd> ShippingAds { get; set; } = new List<ShippingAd>();
        public virtual List<BuyingAd> BuyingAds { get; set; } = new List<BuyingAd>();
        public virtual List<SellingAd> SellingAds { get; set; } = new List<SellingAd>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class ShippingAd
    {
        [JsonIgnore]
        public int ShippingAdId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string OriginPlanetId { get; set; }
        public string OriginPlanetNaturalId { get; set; }
        public string OriginPlanetName { get; set; }

        public string DestinationPlanetId { get; set; }
        public string DestinationPlanetNaturalId { get; set; }
        public string DestinationPlanetName { get; set; }

        public double CargoWeight { get; set; }
        public double CargoVolume { get; set; }

        public string CreatorCompanyId { get; set; }
        public string CreatorCompanyName { get; set; }
        public string CreatorCompanyCode { get; set; }

        public double PayoutPrice { get; set; }
        public string PayoutCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }

    public class BuyingAd
    {
        [JsonIgnore]
        public int BuyingAdId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string CreatorCompanyId { get; set; }
        public string CreatorCompanyName { get; set; }
        public string CreatorCompanyCode { get; set; }

        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }

    public class SellingAd
    {
        [JsonIgnore]
        public int SellingAdId { get; set; }

        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string CreatorCompanyId { get; set; }
        public string CreatorCompanyName { get; set; }
        public string CreatorCompanyCode { get; set; }

        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }
        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }

        public int MaterialAmount { get; set; }

        public double Price { get; set; }
        public string PriceCurrency { get; set; }

        public int DeliveryTime { get; set; }

        public long CreationTimeEpochMs { get; set; }
        public long ExpiryTimeEpochMs { get; set; }

        public string MinimumRating { get; set; }

        [JsonIgnore]
        public int LocalMarketModelId { get; set; }

        [JsonIgnore]
        public virtual LocalMarketModel LocalMarketModel { get; set; }
    }
}

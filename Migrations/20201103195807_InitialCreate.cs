﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FIORest.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AuthenticationModels",
                columns: table => new
                {
                    AuthenticationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AccountEnabled = table.Column<bool>(type: "INTEGER", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", nullable: true),
                    Password = table.Column<string>(type: "TEXT", nullable: true),
                    IsAdministrator = table.Column<bool>(type: "INTEGER", nullable: false),
                    AuthorizationKey = table.Column<Guid>(type: "TEXT", nullable: false),
                    AuthorizationExpiry = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthenticationModels", x => x.AuthenticationModelId);
                });

            migrationBuilder.CreateTable(
                name: "BUIModels",
                columns: table => new
                {
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", nullable: true),
                    Expertise = table.Column<string>(type: "TEXT", nullable: true),
                    Pioneers = table.Column<int>(type: "INTEGER", nullable: false),
                    Settlers = table.Column<int>(type: "INTEGER", nullable: false),
                    Technicians = table.Column<int>(type: "INTEGER", nullable: false),
                    Engineers = table.Column<int>(type: "INTEGER", nullable: false),
                    Scientists = table.Column<int>(type: "INTEGER", nullable: false),
                    AreaCost = table.Column<int>(type: "INTEGER", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIModels", x => x.BUIModelId);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataModels",
                columns: table => new
                {
                    CompanyDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", nullable: true),
                    HighestTier = table.Column<string>(type: "TEXT", nullable: true),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    CountryId = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyCode = table.Column<string>(type: "TEXT", nullable: true),
                    StartingProfile = table.Column<string>(type: "TEXT", nullable: true),
                    StartingLocation = table.Column<string>(type: "TEXT", nullable: true),
                    OverallRating = table.Column<string>(type: "TEXT", nullable: true),
                    ActivityRating = table.Column<string>(type: "TEXT", nullable: true),
                    ReliabilityRating = table.Column<string>(type: "TEXT", nullable: true),
                    StabilityRating = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataModels", x => x.CompanyDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "CXDataModels",
                columns: table => new
                {
                    CXDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    ExchangeName = table.Column<string>(type: "TEXT", nullable: true),
                    ExchangeCode = table.Column<string>(type: "TEXT", nullable: true),
                    Currency = table.Column<string>(type: "TEXT", nullable: true),
                    Previous = table.Column<double>(type: "REAL", nullable: true),
                    Price = table.Column<double>(type: "REAL", nullable: true),
                    PriceTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    High = table.Column<double>(type: "REAL", nullable: true),
                    AllTimeHigh = table.Column<double>(type: "REAL", nullable: true),
                    Low = table.Column<double>(type: "REAL", nullable: true),
                    AllTimeLow = table.Column<double>(type: "REAL", nullable: true),
                    Ask = table.Column<double>(type: "REAL", nullable: true),
                    AskCount = table.Column<int>(type: "INTEGER", nullable: true),
                    Bid = table.Column<double>(type: "REAL", nullable: true),
                    BidCount = table.Column<int>(type: "INTEGER", nullable: true),
                    Supply = table.Column<int>(type: "INTEGER", nullable: true),
                    Demand = table.Column<int>(type: "INTEGER", nullable: true),
                    Traded = table.Column<int>(type: "INTEGER", nullable: true),
                    VolumeAmount = table.Column<double>(type: "REAL", nullable: true),
                    PriceAverage = table.Column<double>(type: "REAL", nullable: true),
                    NarrowPriceBandLow = table.Column<double>(type: "REAL", nullable: true),
                    NarrowPriceBandHigh = table.Column<double>(type: "REAL", nullable: true),
                    WidePriceBandLow = table.Column<double>(type: "REAL", nullable: true),
                    WidePriceBandHigh = table.Column<double>(type: "REAL", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXDataModels", x => x.CXDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "ExpertModels",
                columns: table => new
                {
                    ExpertsModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    AgricultureActive = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    AgricultureEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ResourceExtractioneActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ResourceExtractionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FoodIndustriesActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FoodIndustriesEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ChemistryActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ChemistryEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ConstructionActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ConstructionEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ElectronicsActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ElectronicsEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    FuelRefiningActive = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    FuelRefiningEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    ManufacturingActive = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    ManufacturingEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    MetallurgyActive = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyAvailable = table.Column<int>(type: "INTEGER", nullable: false),
                    MetallurgyEfficiencyGain = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpertModels", x => x.ExpertsModelId);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSModels",
                columns: table => new
                {
                    FLIGHTSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSModels", x => x.FLIGHTSModelId);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureModels",
                columns: table => new
                {
                    InfrastructureModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PopulationId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureModels", x => x.InfrastructureModelId);
                });

            migrationBuilder.CreateTable(
                name: "LocalMarketModels",
                columns: table => new
                {
                    LocalMarketModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MarketId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocalMarketModels", x => x.LocalMarketModelId);
                });

            migrationBuilder.CreateTable(
                name: "MATModels",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    MATModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    CategoryName = table.Column<string>(type: "TEXT", nullable: true),
                    CategoryId = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MATModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PlanetDataModels",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    PlanetDataModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    NaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Namer = table.Column<string>(type: "TEXT", nullable: true),
                    NamingDataEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    Nameable = table.Column<bool>(type: "INTEGER", nullable: false),
                    Gravity = table.Column<double>(type: "REAL", nullable: false),
                    MagneticField = table.Column<double>(type: "REAL", nullable: false),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    MassEarth = table.Column<double>(type: "REAL", nullable: false),
                    OrbitSemiMajorAxis = table.Column<double>(type: "REAL", nullable: false),
                    OrbitEccentricity = table.Column<double>(type: "REAL", nullable: false),
                    OrbitInclination = table.Column<double>(type: "REAL", nullable: false),
                    OrbitRightAscension = table.Column<double>(type: "REAL", nullable: false),
                    OrbitPeriapsis = table.Column<double>(type: "REAL", nullable: false),
                    OrbitIndex = table.Column<int>(type: "INTEGER", nullable: false),
                    Pressure = table.Column<double>(type: "REAL", nullable: false),
                    Radiation = table.Column<double>(type: "REAL", nullable: false),
                    Radius = table.Column<int>(type: "INTEGER", nullable: false),
                    Sunlight = table.Column<double>(type: "REAL", nullable: false),
                    Surface = table.Column<bool>(type: "INTEGER", nullable: false),
                    Temperature = table.Column<double>(type: "REAL", nullable: false),
                    Fertility = table.Column<double>(type: "REAL", nullable: false),
                    HasLocalMarket = table.Column<bool>(type: "INTEGER", nullable: false),
                    HasChamberOfCommerce = table.Column<bool>(type: "INTEGER", nullable: false),
                    HasWarehouse = table.Column<bool>(type: "INTEGER", nullable: false),
                    HasAdministrationCenter = table.Column<bool>(type: "INTEGER", nullable: false),
                    FactionCode = table.Column<string>(type: "TEXT", nullable: true),
                    FactionName = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorId = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorUserName = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationId = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationName = table.Column<string>(type: "TEXT", nullable: true),
                    GovernorCorporationCode = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyName = table.Column<string>(type: "TEXT", nullable: true),
                    CurrencyCode = table.Column<string>(type: "TEXT", nullable: true),
                    CollectorId = table.Column<string>(type: "TEXT", nullable: true),
                    CollectorName = table.Column<string>(type: "TEXT", nullable: true),
                    CollectorCode = table.Column<string>(type: "TEXT", nullable: true),
                    BaseLocalMarketFee = table.Column<double>(type: "REAL", nullable: true),
                    LocalMarketFeeFactor = table.Column<double>(type: "REAL", nullable: true),
                    WarehouseFee = table.Column<double>(type: "REAL", nullable: true),
                    PopulationId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanetDataModels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PRODLinesModels",
                columns: table => new
                {
                    PRODLinesModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SiteId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PRODLinesModels", x => x.PRODLinesModelId);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSModels",
                columns: table => new
                {
                    SHIPSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSModels", x => x.SHIPSModelId);
                });

            migrationBuilder.CreateTable(
                name: "SITESModels",
                columns: table => new
                {
                    SITESModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESModels", x => x.SITESModelId);
                });

            migrationBuilder.CreateTable(
                name: "StorageModels",
                columns: table => new
                {
                    StorageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AddressableId = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    WeightLoad = table.Column<double>(type: "REAL", nullable: false),
                    WeightCapacity = table.Column<double>(type: "REAL", nullable: false),
                    VolumeLoad = table.Column<double>(type: "REAL", nullable: false),
                    VolumeCapacity = table.Column<double>(type: "REAL", nullable: false),
                    FixedStore = table.Column<bool>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageModels", x => x.StorageModelId);
                });

            migrationBuilder.CreateTable(
                name: "UserDataModels",
                columns: table => new
                {
                    UserDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", nullable: true),
                    Tier = table.Column<string>(type: "TEXT", nullable: true),
                    Team = table.Column<bool>(type: "INTEGER", nullable: false),
                    Pioneer = table.Column<bool>(type: "INTEGER", nullable: false),
                    SystemNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetNamingRights = table.Column<int>(type: "INTEGER", nullable: false),
                    IsPayingUser = table.Column<bool>(type: "INTEGER", nullable: false),
                    IsModeratorChat = table.Column<bool>(type: "INTEGER", nullable: false),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    CompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDataModels", x => x.UserDataModelId);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceModels",
                columns: table => new
                {
                    WorkforceModelId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    SiteId = table.Column<string>(type: "TEXT", nullable: true),
                    UserNameSubmitted = table.Column<string>(type: "TEXT", nullable: true),
                    Timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceModels", x => x.WorkforceModelId);
                });

            migrationBuilder.CreateTable(
                name: "FailedLoginAttempt",
                columns: table => new
                {
                    FailedLoginAttemptId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    AuthenticationModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    Address = table.Column<string>(type: "TEXT", nullable: true),
                    FailedAttemptDateTime = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FailedLoginAttempt", x => x.FailedLoginAttemptId);
                    table.ForeignKey(
                        name: "FK_FailedLoginAttempt_AuthenticationModels_AuthenticationModelId",
                        column: x => x.AuthenticationModelId,
                        principalTable: "AuthenticationModels",
                        principalColumn: "AuthenticationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PermissionAllowances",
                columns: table => new
                {
                    PermissionAllowanceId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(type: "TEXT", nullable: true),
                    FlightData = table.Column<bool>(type: "INTEGER", nullable: false),
                    BuildingData = table.Column<bool>(type: "INTEGER", nullable: false),
                    StorageData = table.Column<bool>(type: "INTEGER", nullable: false),
                    ProductionData = table.Column<bool>(type: "INTEGER", nullable: false),
                    WorkforceData = table.Column<bool>(type: "INTEGER", nullable: false),
                    ExpertsData = table.Column<bool>(type: "INTEGER", nullable: false),
                    AuthenticationModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PermissionAllowances", x => x.PermissionAllowanceId);
                    table.ForeignKey(
                        name: "FK_PermissionAllowances_AuthenticationModels_AuthenticationModelId",
                        column: x => x.AuthenticationModelId,
                        principalTable: "AuthenticationModels",
                        principalColumn: "AuthenticationModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIBuildingCosts",
                columns: table => new
                {
                    BUIBuildingCostId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CommodityName = table.Column<string>(type: "TEXT", nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIBuildingCosts", x => x.BUIBuildingCostId);
                    table.ForeignKey(
                        name: "FK_BUIBuildingCosts_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipes",
                columns: table => new
                {
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DurationMs = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipes", x => x.BUIRecipeId);
                    table.ForeignKey(
                        name: "FK_BUIRecipes_BUIModels_BUIModelId",
                        column: x => x.BUIModelId,
                        principalTable: "BUIModels",
                        principalColumn: "BUIModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CompanyDataCurrencyBalances",
                columns: table => new
                {
                    CompanyDataCurrencyBalanceId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CompanyDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyDataCurrencyBalances", x => x.CompanyDataCurrencyBalanceId);
                    table.ForeignKey(
                        name: "FK_CompanyDataCurrencyBalances_CompanyDataModels_CompanyDataModelId",
                        column: x => x.CompanyDataModelId,
                        principalTable: "CompanyDataModels",
                        principalColumn: "CompanyDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXBuyOrders",
                columns: table => new
                {
                    CXBuyOrderId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    ItemCount = table.Column<int>(type: "INTEGER", nullable: true),
                    ItemCost = table.Column<double>(type: "REAL", nullable: false),
                    CXDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXBuyOrders", x => x.CXBuyOrderId);
                    table.ForeignKey(
                        name: "FK_CXBuyOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CXSellOrders",
                columns: table => new
                {
                    CXSellOrderId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    ItemCount = table.Column<int>(type: "INTEGER", nullable: true),
                    ItemCost = table.Column<double>(type: "REAL", nullable: false),
                    CXDataModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CXSellOrders", x => x.CXSellOrderId);
                    table.ForeignKey(
                        name: "FK_CXSellOrders_CXDataModels_CXDataModelId",
                        column: x => x.CXDataModelId,
                        principalTable: "CXDataModels",
                        principalColumn: "CXDataModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlights",
                columns: table => new
                {
                    FLIGHTSFlightId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FlightId = table.Column<string>(type: "TEXT", nullable: true),
                    ShipId = table.Column<string>(type: "TEXT", nullable: true),
                    Origin = table.Column<string>(type: "TEXT", nullable: true),
                    Destination = table.Column<string>(type: "TEXT", nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    FLIGHTSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlights", x => x.FLIGHTSFlightId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlights_FLIGHTSModels_FLIGHTSModelId",
                        column: x => x.FLIGHTSModelId,
                        principalTable: "FLIGHTSModels",
                        principalColumn: "FLIGHTSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InfrastructureInfos",
                columns: table => new
                {
                    InfrastructureInfoId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    Ticker = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Level = table.Column<int>(type: "INTEGER", nullable: false),
                    ActiveLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    CurrentLevel = table.Column<int>(type: "INTEGER", nullable: false),
                    UpkeepStatus = table.Column<double>(type: "REAL", nullable: false),
                    UpgradeStatus = table.Column<double>(type: "REAL", nullable: false),
                    InfrastructureModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfrastructureInfos", x => x.InfrastructureInfoId);
                    table.ForeignKey(
                        name: "FK_InfrastructureInfos_InfrastructureModels_InfrastructureModelId",
                        column: x => x.InfrastructureModelId,
                        principalTable: "InfrastructureModels",
                        principalColumn: "InfrastructureModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BuyingAds",
                columns: table => new
                {
                    BuyingAdId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    PriceCurrency = table.Column<string>(type: "TEXT", nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", nullable: true),
                    LocalMarketModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BuyingAds", x => x.BuyingAdId);
                    table.ForeignKey(
                        name: "FK_BuyingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SellingAds",
                columns: table => new
                {
                    SellingAdId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    PriceCurrency = table.Column<string>(type: "TEXT", nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", nullable: true),
                    LocalMarketModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SellingAds", x => x.SellingAdId);
                    table.ForeignKey(
                        name: "FK_SellingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ShippingAds",
                columns: table => new
                {
                    ShippingAdId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    OriginPlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    OriginPlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    OriginPlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    DestinationPlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    DestinationPlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    DestinationPlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    CargoWeight = table.Column<double>(type: "REAL", nullable: false),
                    CargoVolume = table.Column<double>(type: "REAL", nullable: false),
                    CreatorCompanyId = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyName = table.Column<string>(type: "TEXT", nullable: true),
                    CreatorCompanyCode = table.Column<string>(type: "TEXT", nullable: true),
                    PayoutPrice = table.Column<double>(type: "REAL", nullable: false),
                    PayoutCurrency = table.Column<string>(type: "TEXT", nullable: true),
                    DeliveryTime = table.Column<int>(type: "INTEGER", nullable: false),
                    CreationTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ExpiryTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    MinimumRating = table.Column<string>(type: "TEXT", nullable: true),
                    LocalMarketModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ShippingAds", x => x.ShippingAdId);
                    table.ForeignKey(
                        name: "FK_ShippingAds_LocalMarketModels_LocalMarketModelId",
                        column: x => x.LocalMarketModelId,
                        principalTable: "LocalMarketModels",
                        principalColumn: "LocalMarketModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlanetBuildRequirements",
                columns: table => new
                {
                    PlanetBuildRequirementId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    PlanetDataModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetDataModelId1 = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanetBuildRequirements", x => x.PlanetBuildRequirementId);
                    table.ForeignKey(
                        name: "FK_PlanetBuildRequirements_PlanetDataModels_PlanetDataModelId1",
                        column: x => x.PlanetDataModelId1,
                        principalTable: "PlanetDataModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlanetDataResources",
                columns: table => new
                {
                    PlanetDataResourceId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    ResourceType = table.Column<string>(type: "TEXT", nullable: true),
                    Factor = table.Column<double>(type: "REAL", nullable: false),
                    PlanetDataModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetDataModelId1 = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanetDataResources", x => x.PlanetDataResourceId);
                    table.ForeignKey(
                        name: "FK_PlanetDataResources_PlanetDataModels_PlanetDataModelId1",
                        column: x => x.PlanetDataModelId1,
                        principalTable: "PlanetDataModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlanetProductionFees",
                columns: table => new
                {
                    PlanetProductionFeeId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Category = table.Column<string>(type: "TEXT", nullable: true),
                    FeeAmount = table.Column<double>(type: "REAL", nullable: false),
                    FeeCurrency = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetDataModelId = table.Column<int>(type: "INTEGER", nullable: false),
                    PlanetDataModelId1 = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanetProductionFees", x => x.PlanetProductionFeeId);
                    table.ForeignKey(
                        name: "FK_PlanetProductionFees_PlanetDataModels_PlanetDataModelId1",
                        column: x => x.PlanetDataModelId1,
                        principalTable: "PlanetDataModels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLines",
                columns: table => new
                {
                    ProductionLineId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SiteId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    Capacity = table.Column<int>(type: "INTEGER", nullable: false),
                    Efficiency = table.Column<double>(type: "REAL", nullable: false),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    PRODLinesModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLines", x => x.ProductionLineId);
                    table.ForeignKey(
                        name: "FK_ProductionLines_PRODLinesModels_PRODLinesModelId",
                        column: x => x.PRODLinesModelId,
                        principalTable: "PRODLinesModels",
                        principalColumn: "PRODLinesModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SHIPSShips",
                columns: table => new
                {
                    SHIPSShipId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShipId = table.Column<string>(type: "TEXT", nullable: true),
                    StoreId = table.Column<string>(type: "TEXT", nullable: true),
                    StlFuelStoreId = table.Column<string>(type: "TEXT", nullable: true),
                    FtlFuelStoreId = table.Column<string>(type: "TEXT", nullable: true),
                    Registration = table.Column<string>(type: "TEXT", nullable: true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Manufacturer = table.Column<string>(type: "TEXT", nullable: true),
                    FlightId = table.Column<string>(type: "TEXT", nullable: true),
                    Acceleration = table.Column<double>(type: "REAL", nullable: false),
                    Thrust = table.Column<double>(type: "REAL", nullable: false),
                    Mass = table.Column<double>(type: "REAL", nullable: false),
                    OperatingEmptyMass = table.Column<double>(type: "REAL", nullable: false),
                    ReactorPower = table.Column<double>(type: "REAL", nullable: false),
                    ReactorUtilization = table.Column<double>(type: "REAL", nullable: false),
                    EmitterMinPower = table.Column<double>(type: "REAL", nullable: false),
                    EmitterMaxPower = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    StlFuelFlowRate = table.Column<double>(type: "REAL", nullable: false),
                    SHIPSModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SHIPSShips", x => x.SHIPSShipId);
                    table.ForeignKey(
                        name: "FK_SHIPSShips_SHIPSModels_SHIPSModelId",
                        column: x => x.SHIPSModelId,
                        principalTable: "SHIPSModels",
                        principalColumn: "SHIPSModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESSites",
                columns: table => new
                {
                    SITESSiteId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SiteId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetId = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetIdentifier = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetName = table.Column<string>(type: "TEXT", nullable: true),
                    PlanetFoundedEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    SITESModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESSites", x => x.SITESSiteId);
                    table.ForeignKey(
                        name: "FK_SITESSites_SITESModels_SITESModelId",
                        column: x => x.SITESModelId,
                        principalTable: "SITESModels",
                        principalColumn: "SITESModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StorageItems",
                columns: table => new
                {
                    StorageItemId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialCategory = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialWeight = table.Column<double>(type: "REAL", nullable: false),
                    MaterialVolume = table.Column<double>(type: "REAL", nullable: false),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    TotalWeight = table.Column<double>(type: "REAL", nullable: false),
                    TotalVolume = table.Column<double>(type: "REAL", nullable: false),
                    StorageModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StorageItems", x => x.StorageItemId);
                    table.ForeignKey(
                        name: "FK_StorageItems_StorageModels_StorageModelId",
                        column: x => x.StorageModelId,
                        principalTable: "StorageModels",
                        principalColumn: "StorageModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceDescriptions",
                columns: table => new
                {
                    WorkforceDescriptionId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    WorkforceTypeName = table.Column<string>(type: "TEXT", nullable: true),
                    Population = table.Column<int>(type: "INTEGER", nullable: false),
                    Reserve = table.Column<int>(type: "INTEGER", nullable: false),
                    Capacity = table.Column<int>(type: "INTEGER", nullable: false),
                    Required = table.Column<int>(type: "INTEGER", nullable: false),
                    Satisfaction = table.Column<double>(type: "REAL", nullable: false),
                    WorkforceModelId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceDescriptions", x => x.WorkforceDescriptionId);
                    table.ForeignKey(
                        name: "FK_WorkforceDescriptions_WorkforceModels_WorkforceModelId",
                        column: x => x.WorkforceModelId,
                        principalTable: "WorkforceModels",
                        principalColumn: "WorkforceModelId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeInputs",
                columns: table => new
                {
                    BUIRecipeInputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CommodityName = table.Column<string>(type: "TEXT", nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeInputs", x => x.BUIRecipeInputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeInputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUIRecipeOutputs",
                columns: table => new
                {
                    BUIRecipeOutputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CommodityName = table.Column<string>(type: "TEXT", nullable: true),
                    CommodityTicker = table.Column<string>(type: "TEXT", nullable: true),
                    Weight = table.Column<double>(type: "REAL", nullable: false),
                    Volume = table.Column<double>(type: "REAL", nullable: false),
                    Amount = table.Column<int>(type: "INTEGER", nullable: false),
                    BUIRecipeId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUIRecipeOutputs", x => x.BUIRecipeOutputId);
                    table.ForeignKey(
                        name: "FK_BUIRecipeOutputs_BUIRecipes_BUIRecipeId",
                        column: x => x.BUIRecipeId,
                        principalTable: "BUIRecipes",
                        principalColumn: "BUIRecipeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegments",
                columns: table => new
                {
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Type = table.Column<string>(type: "TEXT", nullable: true),
                    OriginId = table.Column<string>(type: "TEXT", nullable: true),
                    OriginName = table.Column<string>(type: "TEXT", nullable: true),
                    DepartureTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    ArrivalTimeEpochMs = table.Column<long>(type: "INTEGER", nullable: false),
                    FLIGHTSFlightId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegments", x => x.FLIGHTSFlightSegmentId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegments_FLIGHTSFlights_FLIGHTSFlightId",
                        column: x => x.FLIGHTSFlightId,
                        principalTable: "FLIGHTSFlights",
                        principalColumn: "FLIGHTSFlightId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOrders",
                columns: table => new
                {
                    ProductionLineOrderId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CreatedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    StartedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CompletionEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    DurationMs = table.Column<long>(type: "INTEGER", nullable: true),
                    LastUpdatedEpochMs = table.Column<long>(type: "INTEGER", nullable: true),
                    CompletedPercentage = table.Column<double>(type: "REAL", nullable: true),
                    IsHalted = table.Column<bool>(type: "INTEGER", nullable: false),
                    ProductionFee = table.Column<double>(type: "REAL", nullable: false),
                    ProductionFeeCurrency = table.Column<string>(type: "TEXT", nullable: true),
                    ProductionFeeCollectorId = table.Column<string>(type: "TEXT", nullable: true),
                    ProductionFeeCollectorName = table.Column<string>(type: "TEXT", nullable: true),
                    ProductionFeeCollectorCode = table.Column<string>(type: "TEXT", nullable: true),
                    ProductionLineId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOrders", x => x.ProductionLineOrderId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOrders_ProductionLines_ProductionLineId",
                        column: x => x.ProductionLineId,
                        principalTable: "ProductionLines",
                        principalColumn: "ProductionLineId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESBuildings",
                columns: table => new
                {
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BuildingName = table.Column<string>(type: "TEXT", nullable: true),
                    BuildingTicker = table.Column<string>(type: "TEXT", nullable: true),
                    Condition = table.Column<double>(type: "REAL", nullable: false),
                    SITESSiteId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESBuildings", x => x.SITESBuildingId);
                    table.ForeignKey(
                        name: "FK_SITESBuildings_SITESSites_SITESSiteId",
                        column: x => x.SITESSiteId,
                        principalTable: "SITESSites",
                        principalColumn: "SITESSiteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkforceNeeds",
                columns: table => new
                {
                    WorkforceNeedId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Category = table.Column<string>(type: "TEXT", nullable: true),
                    Essential = table.Column<bool>(type: "INTEGER", nullable: false),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    Satisfaction = table.Column<double>(type: "REAL", nullable: false),
                    UnitsPerInterval = table.Column<double>(type: "REAL", nullable: false),
                    UnitsPerOneHundred = table.Column<double>(type: "REAL", nullable: false),
                    WorkforceDescriptionId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkforceNeeds", x => x.WorkforceNeedId);
                    table.ForeignKey(
                        name: "FK_WorkforceNeeds_WorkforceDescriptions_WorkforceDescriptionId",
                        column: x => x.WorkforceDescriptionId,
                        principalTable: "WorkforceDescriptions",
                        principalColumn: "WorkforceDescriptionId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegmentDestinations",
                columns: table => new
                {
                    FLIGHTSFlightSegmentDestinationId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SegmentId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentName = table.Column<string>(type: "TEXT", nullable: true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegmentDestinations", x => x.FLIGHTSFlightSegmentDestinationId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegmentDestinations_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FLIGHTSFlightSegmentOrigins",
                columns: table => new
                {
                    FLIGHTSFlightSegmentOriginId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SegmentId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentNaturalId = table.Column<string>(type: "TEXT", nullable: true),
                    SegmentName = table.Column<string>(type: "TEXT", nullable: true),
                    FLIGHTSFlightSegmentId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FLIGHTSFlightSegmentOrigins", x => x.FLIGHTSFlightSegmentOriginId);
                    table.ForeignKey(
                        name: "FK_FLIGHTSFlightSegmentOrigins_FLIGHTSFlightSegments_FLIGHTSFlightSegmentId",
                        column: x => x.FLIGHTSFlightSegmentId,
                        principalTable: "FLIGHTSFlightSegments",
                        principalColumn: "FLIGHTSFlightSegmentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineInputs",
                columns: table => new
                {
                    ProductionLineInputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductionLineOrderId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineInputs", x => x.ProductionLineInputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineInputs_ProductionLineOrders_ProductionLineOrderId",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductionLineOutputs",
                columns: table => new
                {
                    ProductionLineOutputId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductionLineOrderId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductionLineOutputs", x => x.ProductionLineOutputId);
                    table.ForeignKey(
                        name: "FK_ProductionLineOutputs_ProductionLineOrders_ProductionLineOrderId",
                        column: x => x.ProductionLineOrderId,
                        principalTable: "ProductionLineOrders",
                        principalColumn: "ProductionLineOrderId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESReclaimableMaterials",
                columns: table => new
                {
                    SITESReclaimableMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESReclaimableMaterials", x => x.SITESReclaimableMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESReclaimableMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SITESRepairMaterials",
                columns: table => new
                {
                    SITESRepairMaterialId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    MaterialId = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialName = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialTicker = table.Column<string>(type: "TEXT", nullable: true),
                    MaterialAmount = table.Column<int>(type: "INTEGER", nullable: false),
                    SITESBuildingId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SITESRepairMaterials", x => x.SITESRepairMaterialId);
                    table.ForeignKey(
                        name: "FK_SITESRepairMaterials_SITESBuildings_SITESBuildingId",
                        column: x => x.SITESBuildingId,
                        principalTable: "SITESBuildings",
                        principalColumn: "SITESBuildingId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BUIBuildingCosts_BUIModelId",
                table: "BUIBuildingCosts",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeInputs_BUIRecipeId",
                table: "BUIRecipeInputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipeOutputs_BUIRecipeId",
                table: "BUIRecipeOutputs",
                column: "BUIRecipeId");

            migrationBuilder.CreateIndex(
                name: "IX_BUIRecipes_BUIModelId",
                table: "BUIRecipes",
                column: "BUIModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BuyingAds_LocalMarketModelId",
                table: "BuyingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyDataCurrencyBalances_CompanyDataModelId",
                table: "CompanyDataCurrencyBalances",
                column: "CompanyDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXBuyOrders_CXDataModelId",
                table: "CXBuyOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_CXSellOrders_CXDataModelId",
                table: "CXSellOrders",
                column: "CXDataModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FailedLoginAttempt_AuthenticationModelId",
                table: "FailedLoginAttempt",
                column: "AuthenticationModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlights_FLIGHTSModelId",
                table: "FLIGHTSFlights",
                column: "FLIGHTSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegmentDestinations_FLIGHTSFlightSegmentId",
                table: "FLIGHTSFlightSegmentDestinations",
                column: "FLIGHTSFlightSegmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegmentOrigins_FLIGHTSFlightSegmentId",
                table: "FLIGHTSFlightSegmentOrigins",
                column: "FLIGHTSFlightSegmentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FLIGHTSFlightSegments_FLIGHTSFlightId",
                table: "FLIGHTSFlightSegments",
                column: "FLIGHTSFlightId");

            migrationBuilder.CreateIndex(
                name: "IX_InfrastructureInfos_InfrastructureModelId",
                table: "InfrastructureInfos",
                column: "InfrastructureModelId");

            migrationBuilder.CreateIndex(
                name: "IX_PermissionAllowances_AuthenticationModelId",
                table: "PermissionAllowances",
                column: "AuthenticationModelId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanetBuildRequirements_PlanetDataModelId1",
                table: "PlanetBuildRequirements",
                column: "PlanetDataModelId1");

            migrationBuilder.CreateIndex(
                name: "IX_PlanetDataResources_PlanetDataModelId1",
                table: "PlanetDataResources",
                column: "PlanetDataModelId1");

            migrationBuilder.CreateIndex(
                name: "IX_PlanetProductionFees_PlanetDataModelId1",
                table: "PlanetProductionFees",
                column: "PlanetDataModelId1");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineInputs_ProductionLineOrderId",
                table: "ProductionLineInputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOrders_ProductionLineId",
                table: "ProductionLineOrders",
                column: "ProductionLineId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLineOutputs_ProductionLineOrderId",
                table: "ProductionLineOutputs",
                column: "ProductionLineOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductionLines_PRODLinesModelId",
                table: "ProductionLines",
                column: "PRODLinesModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SellingAds_LocalMarketModelId",
                table: "SellingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_ShippingAds_LocalMarketModelId",
                table: "ShippingAds",
                column: "LocalMarketModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SHIPSShips_SHIPSModelId",
                table: "SHIPSShips",
                column: "SHIPSModelId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESBuildings_SITESSiteId",
                table: "SITESBuildings",
                column: "SITESSiteId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESReclaimableMaterials_SITESBuildingId",
                table: "SITESReclaimableMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESRepairMaterials_SITESBuildingId",
                table: "SITESRepairMaterials",
                column: "SITESBuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_SITESSites_SITESModelId",
                table: "SITESSites",
                column: "SITESModelId");

            migrationBuilder.CreateIndex(
                name: "IX_StorageItems_StorageModelId",
                table: "StorageItems",
                column: "StorageModelId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceDescriptions_WorkforceModelId",
                table: "WorkforceDescriptions",
                column: "WorkforceModelId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkforceNeeds_WorkforceDescriptionId",
                table: "WorkforceNeeds",
                column: "WorkforceDescriptionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BUIBuildingCosts");

            migrationBuilder.DropTable(
                name: "BUIRecipeInputs");

            migrationBuilder.DropTable(
                name: "BUIRecipeOutputs");

            migrationBuilder.DropTable(
                name: "BuyingAds");

            migrationBuilder.DropTable(
                name: "CompanyDataCurrencyBalances");

            migrationBuilder.DropTable(
                name: "CXBuyOrders");

            migrationBuilder.DropTable(
                name: "CXSellOrders");

            migrationBuilder.DropTable(
                name: "ExpertModels");

            migrationBuilder.DropTable(
                name: "FailedLoginAttempt");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegmentDestinations");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegmentOrigins");

            migrationBuilder.DropTable(
                name: "InfrastructureInfos");

            migrationBuilder.DropTable(
                name: "MATModels");

            migrationBuilder.DropTable(
                name: "PermissionAllowances");

            migrationBuilder.DropTable(
                name: "PlanetBuildRequirements");

            migrationBuilder.DropTable(
                name: "PlanetDataResources");

            migrationBuilder.DropTable(
                name: "PlanetProductionFees");

            migrationBuilder.DropTable(
                name: "ProductionLineInputs");

            migrationBuilder.DropTable(
                name: "ProductionLineOutputs");

            migrationBuilder.DropTable(
                name: "SellingAds");

            migrationBuilder.DropTable(
                name: "ShippingAds");

            migrationBuilder.DropTable(
                name: "SHIPSShips");

            migrationBuilder.DropTable(
                name: "SITESReclaimableMaterials");

            migrationBuilder.DropTable(
                name: "SITESRepairMaterials");

            migrationBuilder.DropTable(
                name: "StorageItems");

            migrationBuilder.DropTable(
                name: "UserDataModels");

            migrationBuilder.DropTable(
                name: "WorkforceNeeds");

            migrationBuilder.DropTable(
                name: "BUIRecipes");

            migrationBuilder.DropTable(
                name: "CompanyDataModels");

            migrationBuilder.DropTable(
                name: "CXDataModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlightSegments");

            migrationBuilder.DropTable(
                name: "InfrastructureModels");

            migrationBuilder.DropTable(
                name: "AuthenticationModels");

            migrationBuilder.DropTable(
                name: "PlanetDataModels");

            migrationBuilder.DropTable(
                name: "ProductionLineOrders");

            migrationBuilder.DropTable(
                name: "LocalMarketModels");

            migrationBuilder.DropTable(
                name: "SHIPSModels");

            migrationBuilder.DropTable(
                name: "SITESBuildings");

            migrationBuilder.DropTable(
                name: "StorageModels");

            migrationBuilder.DropTable(
                name: "WorkforceDescriptions");

            migrationBuilder.DropTable(
                name: "BUIModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSFlights");

            migrationBuilder.DropTable(
                name: "ProductionLines");

            migrationBuilder.DropTable(
                name: "SITESSites");

            migrationBuilder.DropTable(
                name: "WorkforceModels");

            migrationBuilder.DropTable(
                name: "FLIGHTSModels");

            migrationBuilder.DropTable(
                name: "PRODLinesModels");

            migrationBuilder.DropTable(
                name: "SITESModels");
        }
    }
}

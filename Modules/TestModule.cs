﻿using System;
using System.Collections.Generic;
using System.Text;

using FIORest.Authentication;

using Nancy;

namespace FIORest.Modules
{
    public class TestModule : NancyModule
    {
        public TestModule() : base("/test")
        {
            this.EnforceAuth();

            Get("/", _ => "Success");
        }
    }
}

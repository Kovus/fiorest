﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class SystemStarsModule : NancyModule
    {
        public SystemStarsModule() : base("/systemstars")
        {
            Post("/", _ =>
            {
                this.EnforceAuthAdmin();
                return PostSystemStars();
            });

            Post("/worldsectors", _ =>
            {
                this.EnforceAuthAdmin();
                return PostWorldSectors();
            });

            Get("/", _ =>
            {
                return GetSystemStars();
            });

            Get("/worldsectors", _ =>
            {
                return GetWorldSectors();
            });
        }

        private Response PostSystemStars()
        {
            using(var req = new FIORequest<JSONRepresentations.SystemStarsdata.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                req.DB.SystemStarsModels.RemoveRange(req.DB.SystemStarsModels);

                var stars = rootObj.payload.message.payload.stars;
                foreach(var star in stars)
                {
                    SystemStarsModel model = new SystemStarsModel();

                    model.SystemId = star.systemId;
                    model.Name = star.address.lines[0].entity.name;
                    model.NaturalId = star.address.lines[0].entity.naturalId;
                    model.Type = star.type;

                    model.PositionX = star.position.x;
                    model.PositionY = star.position.y;
                    model.PositionZ = star.position.z;

                    model.SectorId = star.sectorId;
                    model.SubSectorId = star.subSectorId;

                    foreach(var connection in star.connections)
                    {
                        SystemConnection sysConnection = new SystemConnection();

                        sysConnection.Connection = connection;

                        model.Connections.Add(sysConnection);
                    }

                    req.DB.SystemStarsModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostWorldSectors()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldSectors.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var rootObj = req.JsonPayload;
                req.DB.WorldSectorsModels.RemoveRange(req.DB.WorldSectorsModels);

                var sectors = rootObj.payload.message.payload.sectors;
                foreach( var sector in sectors)
                {
                    WorldSectorsModel model = new WorldSectorsModel();

                    model.SectorId = sector.id;
                    model.Name = sector.name;
                    model.HexQ = sector.hex.q;
                    model.HexR = sector.hex.r;
                    model.HexS = sector.hex.s;
                    model.Size = sector.size;

                    foreach( var subsector in sector.subsectors )
                    {
                        SubSector ss = new SubSector();

                        ss.Id = subsector.id;
                        
                        foreach(var vertex in subsector.vertices)
                        {
                            SubSectorVertex ssv = new SubSectorVertex();

                            ssv.X = vertex.x;
                            ssv.Y = vertex.y;
                            ssv.Z = vertex.z;

                            ss.Vertices.Add(ssv);
                        }

                        model.SubSectors.Add(ss);
                    }

                    req.DB.WorldSectorsModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetSystemStars()
        {
            using (var DB = new PRUNDataContext())
            {
                return JsonConvert.SerializeObject(DB.SystemStarsModels.ToList());
            }
        }

        private Response GetWorldSectors()
        {
            using (var DB = new PRUNDataContext())
            {
                return JsonConvert.SerializeObject(DB.WorldSectorsModels.ToList());
            }
        }
    }
}

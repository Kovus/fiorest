﻿using System.Linq;

using Nancy;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class MaterialModule : NancyModule
    {
        public MaterialModule() : base("/material")
        {
            Post("/", _ =>
            {
                this.EnforceAuth();
                return PostMaterial();
            });

            Get("/{material_ticker}", parameters =>
            {
                return GetMaterial(parameters.material_ticker);
            });

            Get("/category/{category_name}", parameters =>
            {
                return GetMaterialByCategory(parameters.category_name);
            });
        }

        private Response PostMaterial()
        {
            using (var req = new FIORequest<JSONRepresentations.WorldMaterialCategories.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                foreach (var category in data.categories)
                {
                    foreach (var material in category.materials)
                    {
                        MATModel model = req.DB.MATModels.Where(b => b.Ticker == material.ticker).FirstOrDefault();
                        bool bIsAdd = (model == null);
                        if (bIsAdd)
                        {
                            model = new MATModel();
                        }

                        model.CategoryName = category.name;
                        model.CategoryId = category.id;
                        model.Name = material.name;
                        model.Id = material.id;
                        model.Ticker = material.ticker;
                        model.Weight = material.weight;
                        model.Volume = material.volume;

                        model.UserNameSubmitted = req.UserName;
                        model.Timestamp = req.Now;

                        if (bIsAdd)
                        {
                            req.DB.MATModels.Add(model);
                        }
                    }
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response GetMaterial(string MaterialTicker)
        {
            MaterialTicker = MaterialTicker.ToUpper();
            using (var DB = new PRUNDataContext())
            {
                var res = DB.MATModels.Where(m => m.Ticker.ToUpper() == MaterialTicker).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }

        private Response GetMaterialByCategory(string Category)
        {
            Category = Category.ToUpper();
            using(var DB = new PRUNDataContext())
            {
                var res = DB.MATModels.Where(m => m.CategoryName.ToUpper() == Category).ToList();
                if ( res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}

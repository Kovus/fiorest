﻿using System;
using System.Linq;

using Nancy;
using Nancy.Extensions;
using Nancy.IO;

using Newtonsoft.Json;

using FIORest.Authentication;
using FIORest.Database;
using FIORest.Database.Models;

namespace FIORest.Modules
{
    public class CompanyModule : NancyModule
    {
        public CompanyModule() : base("/company")
        {
            this.EnforceAuth();

            Post("/", _ =>
            {
                return PostCompany();
            });

            Post("/data", _ =>
            {
                return PostCompanyData();
            });

            Get("/{company_id}", parameters =>
            {
                return GetCompany(parameters.company_id);
            });
        }

        private Response PostCompany()
        {
            using (var req = new FIORequest<JSONRepresentations.CompanyData.Rootobject>(Request))
            {
                if (req.BadRequest)
                {
                    return req.ReturnBadRequest();
                }

                var data = req.JsonPayload.payload.message.payload;
                var model = req.DB.CompanyDataModels.Where(c => c.CompanyId == data.id).FirstOrDefault();
                bool bIsAdd = (model == null);
                if (bIsAdd)
                {
                    model = new CompanyDataModel();
                }
                else
                {
                    model.Balances.Clear();
                }

                model.CompanyId = data.id;
                model.CompanyName = data.name;
                model.CountryId = data.countryId;

                model.CurrencyCode = data.ownCurrency.code;

                model.StartingProfile = data.startingProfile;
                model.StartingLocation = data.startingLocation.lines[1].entity.naturalId;

                foreach (var account in data.currencyAccounts)
                {
                    CompanyDataCurrencyBalance balance = new CompanyDataCurrencyBalance();

                    balance.Currency = account.currencyBalance.currency;
                    balance.Balance = account.currencyBalance.amount;

                    model.Balances.Add(balance);
                }

                model.OverallRating = data.ratingReport.overallRating;
                foreach (var subRating in data.ratingReport.subRatings)
                {
                    switch (subRating.score)
                    {
                        case "ACTIVITY":
                            model.ActivityRating = subRating.rating;
                            break;
                        case "RELIABILITY":
                            model.ReliabilityRating = subRating.rating;
                            break;
                        case "STABILITY":
                            model.StabilityRating = subRating.rating;
                            break;
                    }
                }

                model.UserNameSubmitted = req.UserName;
                model.Timestamp = req.Now;

                if (bIsAdd)
                {
                    req.DB.CompanyDataModels.Add(model);
                }

                req.DB.SaveChanges();
                return HttpStatusCode.OK;
            }
        }

        private Response PostCompanyData()
        {
            using (var reqStream = RequestStream.FromStream(Request.Body))
            {
                var body = reqStream.AsString();
                try
                {
                    var rootObj = JsonConvert.DeserializeObject<JSONRepresentations.CompanyDataData.Rootobject>(body);
                    if (rootObj != null)
                    {
                        var data = rootObj.payload.message.payload;

                        string UserName = Request.GetUserName();
                        DateTime now = DateTime.Now.ToUniversalTime();

                        using (var DB = new PRUNDataContext())
                        {
                            var model = DB.CompanyDataModels.Where(c => c.CompanyId == data.body.id).FirstOrDefault();
                            bool bIsAdd = (model == null);
                            if (bIsAdd)
                            {
                                model = new CompanyDataModel();
                            }

                            model.UserName = data.body.username;
                            model.HighestTier = data.body.highestTier;
                            model.Pioneer = data.body.pioneer;
                            model.Team = data.body.team;

                            model.CreatedEpochMs = data.body.created.timestamp;

                            model.CompanyId = data.body.company.id;
                            model.CompanyName = data.body.company.name;
                            model.CompanyCode = data.body.company.code;

                            model.UserNameSubmitted = UserName;
                            model.Timestamp = now;

                            if (bIsAdd)
                            {
                                DB.CompanyDataModels.Add(model);
                            }

                            DB.SaveChanges();
                            return HttpStatusCode.OK;
                        }
                    }
                }
                catch(Exception ex)
                {
                    return Utils.ReturnBadResponse(Request, ex);
                }
                
                return HttpStatusCode.BadRequest;
            }
        }

        private Response GetCompany(string CompanyId)
        {
            using (var DB = new PRUNDataContext())
            {
                var res = DB.CompanyDataModels.Where(c => c.CompanyId == CompanyId).FirstOrDefault();
                if (res != null)
                {
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return HttpStatusCode.NoContent;
                }
            }
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using FIORest.Database;
using FIORest.Database.Models;

using Microsoft.AspNetCore.Hosting;
using Mono.Options;


namespace FIORest
{
    class Program
    {
        private static bool ShouldShowHelp = false;

        static void Main(string[] args)
        {
            OptionSet options = new OptionSet
            {
                {
                    "v",
                    "Increase debug message verbosity",
                    v =>
                    {
                        if (v != null)
                        {
                            ++Globals.Opts.VerbosityLevel;
                        }
                    }
                },
                {
                    "onlyhttp",
                    "If we should only allow http (disable https)",
                    onlyhttp =>
                    {
                        if (onlyhttp != null)
                        {
                            Globals.Opts.OnlyHttp = true;
                        }
                    }
                },
                {
                    "databasefilepath=",
                    "The path to store the sqlite database",
                    databasefilepath =>
                    {
                        if (databasefilepath != null)
                        {
                            Globals.Opts.DatabaseFilePath = databasefilepath;
                        }
                    }
                },
                {
                    "badrequestpath=",
                    "The path to store bad requests",
                    badrequestpath =>
                    {
                        if (badrequestpath != null)
                        {
                            Globals.Opts.BadRequestPath = badrequestpath;
                        }
                    }
                },
                {
                    "certpath=",
                    "The path to the pfx",
                    certfilepath =>
                    {
                        if (certfilepath != null)
                        {
                            Globals.Opts.CertFilePath = certfilepath;
                        }
                    }
                },
                {
                    "certpassword=",
                    "The password for the cert",
                    certpassword =>
                    {
                        if (certpassword != null)
                        {
                            Globals.Opts.CertPassword = certpassword;
                        }
                    }
                },
                {
                    "updatedirectory=",
                    "Path to the update directory",
                    updatedirectory =>
                    {
                        if (updatedirectory != null)
                        {
                            Globals.Opts.UpdateDirectory = updatedirectory;
                        }
                    }
                },
                {
                    "h|help",
                    "Show this message and exit",
                    h =>
                    {
                        ShouldShowHelp = (h != null);
                    }
                }
            };

            options.Parse(args);

            if (ShouldShowHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                Environment.Exit(0);
            }

            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseKestrel(options =>
                {
                    options.AllowSynchronousIO = true;
                    if ( Globals.Opts.OnlyHttp )
                    {
                        Console.Out.WriteLine("HTTPS is disabled");
                        options.ListenAnyIP(4443);
                    }
                    else 
                    {
                        options.ListenAnyIP(443, listenOptions =>
                        {
                            Console.Out.WriteLine("Using HTTPS");
                            if (!String.IsNullOrWhiteSpace(Globals.Opts.CertFilePath))
                            {
                                X509Certificate2Collection coll = new X509Certificate2Collection();
                                coll.Import(File.ReadAllBytes(Globals.Opts.CertFilePath));

                                if (!String.IsNullOrWhiteSpace(Globals.Opts.CertPassword))
                                {
                                    listenOptions.UseHttps(Globals.Opts.CertFilePath, Globals.Opts.CertPassword);
                                }
                                else
                                {
                                    listenOptions.UseHttps(coll[0]);
                                }
                            }
                            else
                            {
                                listenOptions.UseHttps();
                            }
                            
                        });
                    }
                })
                .UseStartup<Startup>()
                .Build();

            try
            {
                host.RunAsync();
            }
            catch (Exception)
            {
                Console.Error.WriteLine("Failed to start server");
            }

            Console.Out.WriteLine("Server started.");
            PrintHelp();

            ConsoleKeyInfo k;
            do
            {
                Console.Out.Write("\b \b");
                k = Console.ReadKey();
                Console.Out.Write("\b \b");
                switch (k.KeyChar)
                {
                    case 'c':
                        CreateUser();
                        break;
                    case 'h':
                        PrintHelp();
                        break;
                }
            } while (k.KeyChar != 'q');

            host.StopAsync();
        }

        // Not cryptographically secure, but who cares.
        private static Random rnd = new Random();
        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_-=+/\\'\"<>,.`~";
            StringBuilder res = new StringBuilder();
            while(0 < length--)
            {
                res.Append(validCharacters[rnd.Next(validCharacters.Length)]);
            }

            return res.ToString();
        }

        private static void CreateUser()
        {
            Console.Out.WriteLine("Enter UserName: ");
            string username = Console.ReadLine().Trim();
            if ( username.Length == 0 )
            {
                Console.Error.WriteLine("Empty username specified. Canceling user create.");
                return;
            }


            string preMadeRandomPassword = GenerateRandomPassword(16);
            Console.Out.WriteLine($"Enter Password: [{preMadeRandomPassword}] ");
            string password = Console.ReadLine();
            if ( password.Length == 0)
            {
                // Use the pre-made
                password = preMadeRandomPassword;
            }
            else if ( password.Length < 8)
            {
                Console.Error.WriteLine("Password with length less than 8 specified.  Canceling user create.");
                return;
            }

            bool isAdmin = false;
            Console.Out.WriteLine("User Administrator (y/n): [n] ");
            string isAdminInput = Console.ReadLine().Trim().ToLower();
            if (isAdminInput.Length > 0 && isAdminInput[0] == 'y')
            {
                isAdmin = true;
            }

            bool exitingUser = false;
            using (var DB = new PRUNDataContext())
            {
                var authModel = DB.AuthenticationModels.Where(e => e.UserName == username).FirstOrDefault();
                if (authModel != null)
                {
                    exitingUser = true;
                    Console.Out.WriteLine("User already exists.  Overwrite? (y/n): [n] ");
                    string overwriteInput = Console.ReadLine().Trim().ToLower();
                    if (overwriteInput.Length == 0 || overwriteInput[0] == 'n')
                    {
                        Console.Error.WriteLine("Canceling user create.");
                        return;
                    }
                }

                if (authModel == null)
                {
                    authModel = new AuthenticationModel();
                }

                authModel.AccountEnabled = true;
                authModel.UserName = username;
                authModel.Password = password;
                authModel.IsAdministrator = isAdmin;

                if (!exitingUser)
                {
                    DB.AuthenticationModels.Add(authModel);
                }
                DB.SaveChanges();
                Console.Out.WriteLine("User created.");
            }
        }

        private static void PrintHelp()
        {
            Console.Out.WriteLine("Press 'c' to create a user.");
            Console.Out.WriteLine("Press 'h' to display this help.");
            Console.Out.WriteLine("Press 'q' to quit.\n");
        }
    }
}
